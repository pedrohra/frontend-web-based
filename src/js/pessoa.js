export class Pessoa {
  constructor(nome, dataNascimento, documento, sexo, endereco) { // definição do construtor para as variáveis do sistema
    this.nome = nome;
    this.dataNascimento = dataNascimento;
    this.documento = documento;
    this.sexo = sexo;
    this.endereco = endereco;
  }

  getNome() {
    return this.nome;
  }

  setNome(nome){
    this.nome=nome;
  }

  getDataNascimento(){
    return this.dataNascimento;
  }

  setDataNascimento(dataNascimento){
    this.dataNascimento=dataNascimento;
  }

  getDocumento(){
    return this.documento;
  }

  setDocumento(documento){
    this.documento=documento;
  }

  getSexo(){
    return this.sexo;
  }

  setSexo(sexo){
    this.sexo=sexo;
  }

  getEndereco(){
    return this.endereco;
  }
 
  setEndereco(endereco){
    this.endereco=endereco;
  }
  
}
