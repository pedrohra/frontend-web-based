import {Pessoa} from './pessoa.js'; // importa módulo responsável pela classe Pessoa do arquivo pessoa.js

// inicio do bloco de declaração das variáveis globais
let pessoasCadastradas = []; // array com pessoas cadastradas
let indiceAlt; // variável que armazena indice dos dados modificados
let col1, col2, col3, col4, col5; // variáveis para criação de novas colunas das tabelas do site
let _0a9=0, _10a19=0, _20a29=0, _30a39=0, mais40=0, contMasc=0, contFem=0, objetoChart; /* variáveis de preenchimento 
de dados dos gráficos de relatórios */
let graficoFaixaEtaria = document.getElementById('relatorioFaixaEtaria').getContext('2d');/* variável para criação do 
gráfico de relatório de quantidade de pessoas por faixa etária */
let graficoSexo = document.getElementById('relatorioSexo').getContext('2d'); /* variável para criação do gráfico
 de relatório de quantidade de pessoas por sexo */
Chart.defaults.global.defaultFontFamily = 'Lato'; // definição de fonte para os gráficos
Chart.defaults.global.defaultFontSize = 18; // definição de tamanho de fonte para os gráficos
Chart.defaults.global.defaultFontColor = '#777'; // definição de cor de fonte para os gráficos
// fim do bloco de declaração das variáveis globais


// inicio de event listener em JQuery para busca de CEP
$(window).ready(function() {
	// limpa campos ao não encontrar endereço para o CEP digitado
    function limpa_formulário_cep() {
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
    }
    // event listener onblur para busca de CEP com retorno em JSON
    $("#cep").blur(function() {
	    var cep = $(this).val().replace(/\D/g, '');
	    if (cep != "") {
	        var validacep = /^[0-9]{8}$/;
	        if(validacep.test(cep)) {
	            $("#rua").val("...");
	            $("#bairro").val("...");
	            $("#cidade").val("...");
	            $("#uf").val("...");
	            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados){
		            if (!("erro" in dados)) {
		                $("#rua").val(dados.logradouro);
		                $("#bairro").val(dados.bairro);
		                $("#cidade").val(dados.localidade);
		                $("#uf").val(dados.uf);
		            }
		            else{
		                limpa_formulário_cep();
		                alert("CEP não encontrado.");
		            }
	    		});
	    	}
		    else{
		        limpa_formulário_cep();
		        alert("Formato de CEP inválido.");
		    }
	    }
		else{
		    limpa_formulário_cep();
		}
    });
});

// inicio de event listener para determinar marcação em checkbox da função de cadastramento de pessoas
window.addEventListener('click', function(e){
	if (document.getElementById('checkboxMasc').contains(e.target)){
		document.getElementById("checkboxMasc").checked=true;
		document.getElementById("checkboxFem").checked=false;
	}
	else if (document.getElementById('checkboxFem').contains(e.target)){
		document.getElementById("checkboxFem").checked=true;
		document.getElementById("checkboxMasc").checked=false;
	}
	
});

// inicio de event listener que modifica documentos ao carregar página
window.addEventListener('load', function(){
	document.getElementById("checkboxMasc").checked=true;
	document.getElementById("botaoAdd").disabled=true;
});

// função responsável pela verificação se os caracteres digitados são letras
export function validarAlpha(id){
    var textInput = document.getElementById(id).value;
    textInput = textInput.replace(/[^A-Za-z ]/g, "");
    document.getElementById(id).value = textInput;
}

// função responsável pela verificação se os caracteres digitados são números
export function validarNumero(id){
    var textInput = document.getElementById(id).value;
    textInput = textInput.replace(/[^0-9]/g, "");
    document.getElementById(id).value = textInput;
}

// função responsável pela verificação e remoção de pessoa cadastrada no sistema
export function verificarRmv(){
	let tabelaRmv=document.getElementById("tabelaRmv");
	if(tabelaRmv.rows.length>1&&document.getElementById("rgCpfRmv").value==tabelaRmv.rows[1].cells[2].innerHTML){
		return false;
	}
	else{
		return true;
	}
}

// função responsável pela verificação e alteração de dados de pessoa cadastrada no sistema
export function verificarAlt(){
	let tabelaAlt=document.getElementById("tabelaAlt");
	if(tabelaAlt.rows.length>1&&document.getElementById("rgCpfAlt").value==tabelaAlt.rows[1].cells[2].innerHTML){
		return false;
	}
	else{
		return true;
	}
}

// função responsável pela verificação e consulta de pessoa cadastrada no sistema
export function verificarPsq(){
	let tabelaPsq=document.getElementById("tabelaPsq");
	if(tabelaPsq.rows.length>1&&tabelaPsq.rows.length<3&&
		document.getElementById("rgCpfBusca").value==tabelaPsq.rows[1].cells[2].innerHTML){
		return false;
	}
	else{
		return true;
	}
}

// função responsável por limpar valores da área de cadastramento de pessoas
export function limparCamposAdd(){
	document.getElementById("nomeCompleto").value="";
	document.getElementById("documento").value="";
	document.getElementById("dataNascimento").value="";
	document.getElementById("cep").value="";
	document.getElementById("rua").value="";
	document.getElementById("numero").value="";
	document.getElementById("bairro").value="";
	document.getElementById("cidade").value="";
	document.getElementById("uf").value="";
	document.getElementById("checkboxMasc").checked=true;
	document.getElementById("checkboxFem").checked=false;
}

// função responsável por limpar valores da área de remoção de pessoas
export function limparCamposRmv(){
	document.getElementById("rgCpfRmv").value="";
	let tabelaRmv=document.getElementById("tabelaRmv");
	if(tabelaRmv.rows.length>1){
		tabelaRmv.deleteRow(1);
	}
}

// função responsável por limpar valores da área de alteção de dados de pessoas
export function limparCamposAlt(){
	document.getElementById("rgCpfAlt").value="";
	let tabelaAlt=document.getElementById("tabelaAlt");
	if(tabelaAlt.rows.length>1){
		tabelaAlt.deleteRow(1);
	}
}

// função responsável por limpar valores da área de consulta de pessoas
export function limparCamposPsq(){
	document.getElementById("rgCpfBusca").value="";
	let tabelaPsq=document.getElementById("tabelaPsq");
	let linhaTabela = tabelaPsq.getElementsByTagName('tr');
	let numLinha = linhaTabela.length;

	for (var x=numLinha-1; x>0; x--) {
	   tabelaPsq.deleteRow(x);
	}
}

// função responsável por limpar valores da área de relatórios do sistema
export function limparCamposRelatorio(){
	_0a9=0;
	_10a19=0;
	_20a29=0;
	_30a39=0;
	mais40=0;
	contMasc=0;
	contFem=0;
	if(objetoChart!=undefined){
		objetoChart.destroy();
	}
}

// função responsável por reunir todas funções de limpeza de valores do sistema
export function limparCampos(){
	limparCamposAdd();
	limparCamposRmv();
	limparCamposAlt();
	limparCamposPsq();
	limparCamposRelatorio();
}

// função responsável por verificar e autocompletar CPF digitado
export function verificarCpf(documento){
	let cpfDigitado = document.getElementById(documento).value.length;
	var textInput = document.getElementById(documento).value;
    textInput = textInput.replace(/[^0-9.-]/g, "");
    document.getElementById(documento).value = textInput;
	if(cpfDigitado == 3){
		document.getElementById(documento).value +=".";
	}
	else if(cpfDigitado == 7){
		document.getElementById(documento).value +=".";
	}
	else if(cpfDigitado == 11){
		document.getElementById(documento).value +="-";
	}
}

// função responsável por limpar campos, verificar área a ser exibida/escondida ao clicar em botão do site 
export function verificarBotao(id){
	limparCampos(); // limpa todos valores do sistema

	if(id=="botaoAdd"){ // exibe área de cadastramento
		document.getElementById("botaoAdd").disabled = true;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoRelatorio").disabled = false;
		document.getElementById("botaoFaixaEtaria").disabled = false;
		document.getElementById("adicionar").style.display="block";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="none";
	}
	else if(id=="botaoRmv"){ // exibe área de remoção
		document.getElementById("botaoRmv").disabled = true;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoRelatorio").disabled = false;
		document.getElementById("botaoFaixaEtaria").disabled = false;
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="block";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="none";
	}
	else if(id=="botaoAlt"){ // exibe área de modificação de dados
		document.getElementById("botaoAlt").disabled = true;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoRelatorio").disabled = false;
		document.getElementById("botaoFaixaEtaria").disabled = false;
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="block";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="none";
	}
	else if(id=="botaoPsq"){ // exibe área de consulta
		document.getElementById("botaoPsq").disabled = true;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoRelatorio").disabled = false;
		document.getElementById("botaoFaixaEtaria").disabled = false;
		listarPessoas(1,"tabelaPsq"); // lista todas pessoas cadastradas no sistema
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="block";
		document.getElementById("relatorio").style.display="none";
	}
	else  if(id=="botaoRelatorio"){ // exibe área de relatórios
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoRelatorio").disabled = true;
		document.getElementById("botaoFaixaEtaria").disabled = true;
		document.getElementById("botaoGenero").disabled = false;
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="block";
		document.getElementById("relatorioFaixaEtaria").style.display="block";
		document.getElementById("relatorioSexo").style.display="none";
		relatorioFaixaEtaria();
	}
	else if(id=="botaoFaixaEtaria"){ // exibe gráfico do relatório de quantidade de pessoas por faixa etária
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoRelatorio").disabled = true;
		document.getElementById("botaoFaixaEtaria").disabled = true;
		document.getElementById("botaoGenero").disabled = false;
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="block";
		document.getElementById("relatorioFaixaEtaria").style.display="block";
		document.getElementById("relatorioSexo").style.display="none";
		relatorioFaixaEtaria();
	}
	else{ // exibe gráfico do relatório de quantidade de pessoas por sexo
		document.getElementById("botaoAlt").disabled = false;
		document.getElementById("botaoAdd").disabled = false;
		document.getElementById("botaoRmv").disabled = false;
		document.getElementById("botaoPsq").disabled = false;
		document.getElementById("botaoRelatorio").disabled = true;
		document.getElementById("botaoFaixaEtaria").disabled = false;
		document.getElementById("botaoGenero").disabled = true;
		document.getElementById("adicionar").style.display="none";
		document.getElementById("remover").style.display="none";
		document.getElementById("alterar").style.display="none";
		document.getElementById("listar").style.display="none";
		document.getElementById("relatorio").style.display="block";
		document.getElementById("relatorioFaixaEtaria").style.display="none";
		document.getElementById("relatorioSexo").style.display="block";
		relatorioSexo();
	}
}

// função responsável verificar se CPF já está cadastrado no sistema ao cadastrar pessoa
export function verificarCpfExistente(cpfCadastro){
	for (var i = 0; i < pessoasCadastradas.length; i++) {
		if(pessoasCadastradas[i].documento == cpfCadastro){
			alert("CPF já consta no sistema!");
			return true;
		}
	}
	return false;
}

// função responsável por cadastrar pessoas no sistema
export function adicionarPessoa() {
	if(document.getElementById("nomeCompleto").value!=""&&document.getElementById("dataNascimento").value!=""&& 
		document.getElementById("documento").value.length==document.getElementById("documento").maxLength){/*campos 
		obrigatórios preenchidos corretamente*/
		if(!verificarCpfExistente(document.getElementById("documento").value)){ // verifica se existe CPF cadastrado
			let sexo;
			let endereco;
			let data=document.getElementById("dataNascimento").value;
			let ano=data.substring(0,4);
			let mes=data.substring(5,7);
			let dia=data.substring(8,10);
			data=dia+"/"+mes+"/"+ano;
			if(document.getElementById("cep").value!=""){
				endereco=document.getElementById("rua").value+", "+document.getElementById("numero").value+", "+
				document.getElementById("bairro").value+", "+document.getElementById("cidade").value+", "+
				document.getElementById("uf").value;
			}
			else{
				endereco="Não informado";
			}

			if(document.getElementById("checkboxMasc").checked){
				sexo="Masculino";
			}
			else{
				sexo="Feminino";
			}
			let pessoa = new Pessoa(document.getElementById("nomeCompleto").value, data, 
				document.getElementById("documento").value, sexo, endereco); /* instancia objeto da classe Pessoa com os 
			valores	preenchidos na área de cadastramento */
			pessoa.endereco=endereco;
			pessoasCadastradas[pessoasCadastradas.length] = pessoa;/* preenche array de pessoas cadastradas com objeto 
			instanciado*/
			alert("Pessoa cadastrada com sucesso!");
			limparCamposAdd();
		}
	}
	else{ // campos obrigatórios não preenchidos ou preenchidos incorretamente
		alert("Preencha os campos obrigatórios!");
	}
}

// função responsável por remover pessoas do sistema
export function removerPessoa(){
	for (var i = 0; i < pessoasCadastradas.length; i++) {
			if(pessoasCadastradas[i].getDocumento() == document.getElementById("rgCpfRmv").value){
				pessoasCadastradas.splice(i, 1);
				alert("Pessoa removida com sucesso!");
				return true;
			}
	}
	alert("Não há pessoa com esse RG/CPF!");
	return false;
}

// função responsável alterar dados de pessoa cadastrada no sistema
export function alterarPessoa(){
	let tabela = document.getElementById("tabelaAlt");
	pessoasCadastradas[indiceAlt].nome=tabela.rows[1].cells[0].innerHTML;
	pessoasCadastradas[indiceAlt].dataNascimento=tabela.rows[1].cells[1].innerHTML;
	pessoasCadastradas[indiceAlt].documento=tabela.rows[1].cells[2].innerHTML;
	pessoasCadastradas[indiceAlt].sexo=tabela.rows[1].cells[3].innerHTML;
	pessoasCadastradas[indiceAlt].endereco=tabela.rows[1].cells[4].innerHTML;
	alert("Dados alterados com sucesso!");
}

// função responsável por listar ou consultar pessoas cadastradas no sistema
export function listarPessoas(opcao,tabelaOpc){
	if(opcao == 1){ // lista todas pessoas cadastradas
		for (var i = 0; i < pessoasCadastradas.length; i++) {
			let tabela = document.getElementById("tabelaPsq");
			let numLinhas = tabela.rows.length;
			let linha = tabela.insertRow(numLinhas);
			col1 = linha.insertCell(0);
			col2 = linha.insertCell(1);
			col3 = linha.insertCell(2);
			col4 = linha.insertCell(3);
			col5 = linha.insertCell(4);
			col1.innerHTML = pessoasCadastradas[i].nome;
			col2.innerHTML = pessoasCadastradas[i].dataNascimento;
			col3.innerHTML = pessoasCadastradas[i].documento;
			col4.innerHTML = pessoasCadastradas[i].sexo;
			col5.innerHTML = pessoasCadastradas[i].endereco;
		}
	}
	else{ // consulta de pessoa cadastrada no sistema por CPF
		for (var i = 0; i < pessoasCadastradas.length; i++) {
			let rgCpf;
			let tabela;
			if(tabelaOpc == "tabelaPsq"){ // define tabela a ser preenchida para tabela da área de consulta
				rgCpf="rgCpfBusca";
				tabela = document.getElementById("tabelaPsq");

			}
			else if(tabelaOpc == "tabelaRmv"){ // define tabela a ser preenchida para tabela da área de remoção
				rgCpf="rgCpfRmv";
				tabela = document.getElementById("tabelaRmv");
			}
			else if(tabelaOpc == "tabelaAlt"){ // define tabela a ser preenchida para tabela da área de alteração
				rgCpf="rgCpfAlt";
				tabela = document.getElementById("tabelaAlt");
			}
			if(verificarRmv()&&tabelaOpc == "tabelaRmv"||verificarAlt()&&tabelaOpc == "tabelaAlt"||verificarPsq()
				&&tabelaOpc == "tabelaPsq"){ // verifica se dado consultado já está presente na tabela
				if(pessoasCadastradas[i].getDocumento() == document.getElementById(rgCpf).value){//verifica se existe CPF
					limparCamposPsq(); //limpa valores presentes na tabela
					let numLinhas = tabela.rows.length;
					let linha = tabela.insertRow(numLinhas);
					col1 = linha.insertCell(0);
					col2 = linha.insertCell(1);
					col3 = linha.insertCell(2);
					col4 = linha.insertCell(3);
					col5 = linha.insertCell(4);
					if(tabelaOpc == "tabelaAlt"){
						col1.contentEditable = "true";		
						col2.id = "dataAlt";
						col2.contentEditable = "true";
						col3.id = "documentoAlt";
						col3.contentEditable = "true";
						col4.contentEditable = "true";
						col5.contentEditable = "true";
						indiceAlt=i;
					}
					col1.innerHTML = pessoasCadastradas[i].nome;
					col2.innerHTML = pessoasCadastradas[i].dataNascimento;
					col3.innerHTML = pessoasCadastradas[i].documento;
					col4.innerHTML = pessoasCadastradas[i].sexo;
					col5.innerHTML = pessoasCadastradas[i].endereco;
					
				}
				else{
					alert("CPF não encontrado!");
				}
			}
		}
	}
	
}

// função responsável pela criação do gráfico de quantidade de pessoas por faixa etária da área de relatórios com Chart.js
export function relatorioFaixaEtaria(){
	var hoje = new Date();
	var ano = hoje.getFullYear();
	for(let i=0; i < pessoasCadastradas.length; i++){ // percorre array de pessoas cadastradas
		if((ano-pessoasCadastradas[i].dataNascimento.substring(6,10))>=0&&
			(ano-pessoasCadastradas[i].dataNascimento.substring(6,10))<=9){ // verifica idade
			_0a9=_0a9+1; // adiciona quantidade na faixa etária de 0 a 9 anos 
		}
		else if((ano-pessoasCadastradas[i].dataNascimento.substring(6,10))>=10&&
			(ano-pessoasCadastradas[i].dataNascimento.substring(6,10))<=19){ // verifica idade
			_10a19=_10a19+1; // adiciona quantidade na faixa etária de 10 a 19 anos
		}
		else if((ano-pessoasCadastradas[i].dataNascimento.substring(6,10))>=20&&
			(ano-pessoasCadastradas[i].dataNascimento.substring(6,10))<=29){ // verifica idade
			_20a29=_20a29+1; // adiciona quantidade na faixa etária de 20 a 29 anos
		}
		else if((ano-pessoasCadastradas[i].dataNascimento.substring(6,10))>=30&&
			(ano-pessoasCadastradas[i].dataNascimento.substring(6,10))<=39){ // verifica idade
			_30a39=_30a39+1; // adiciona quantidade na faixa etária de 30 a 39 anos
		}
		if((ano-pessoasCadastradas[i].dataNascimento.substring(6,10))>=40){ // verifica idade
			mais40=mais40+1; // adiciona quantidade na faixa etária de mais de 40 anos
		}
	}
	objetoChart = new Chart(graficoFaixaEtaria, { // instancia de objeto do gráfico
	    type:'bar', // tipos de gráfico: bar, horizontalBar, pie, line, doughnut, radar, polarArea
	    data:{ // inicio do bloco de dados
	        labels:['0 a 9 anos', '10 a 19 anos', '20 a 29 anos', '30 a 39 anos', 'Mais de 40 anos'],
	        datasets:[{
	        	label:'Quantidade',
	        	data:[_0a9,_10a19,_20a29,_30a39,mais40], // quantidades encontradas para cada faixa etária
	        	backgroundColor:[
		            'rgba(255, 99, 132, 0.6)',
		            'rgba(54, 162, 235, 0.6)',
		            'rgba(255, 206, 86, 0.6)',
		            'rgba(75, 192, 192, 0.6)',
		            'rgba(153, 102, 255, 0.6)',
		            'rgba(255, 159, 64, 0.6)',
		            'rgba(255, 99, 132, 0.6)'
	          	],
	            borderWidth:1,
	        	borderColor:'#777',
	        	hoverBorderWidth:3,
	        	hoverBorderColor:'#000'
	        }]
	    },
	    options:{ // opções de definições do gráfico
	        title:{
	        	display:true,
	        	text:'Faixa etária das pessoas cadastradas',
	        	fontSize:25
	        },
	        legend:{
	        	display:false,
	         	position:'bottom',
	         	labels:{
	        		fontColor:'#000'
	          	}
	        },
	        layout:{
	        	padding:{
	        		left:50,
	            	right:0,
	            	bottom:0,
	            	top:0
	          	}
	        },
        	tooltips:{
        		enabled:true
    		}
    	}
	});
}

// função responsável pela criação do gráfico de quantidade de pessoas por sexo da área de relatórios com Chart.js
export function relatorioSexo(){
	for(let i=0; i < pessoasCadastradas.length; i++){ // percorre array de pessoas cadastradas
		if(pessoasCadastradas[i].sexo=="Masculino"){ // verifica sexo
			contMasc=contMasc+1; // adiciona quantidade de pessoas com sexo 'Masculino'
		}
		else{
			contFem=contFem+1; // adiciona quantidade de pessoas com sexo 'Feminino'
		}
	}
	objetoChart = new Chart(graficoSexo, { // instancia de objeto do gráfico
	    type:'bar', // tipos de gráfico: bar, horizontalBar, pie, line, doughnut, radar, polarArea
	    data:{ // inicio do bloco de dados
	        labels:['Masculino', 'Feminino'],
	        datasets:[{
	        	label:'Quantidade',
	        	data:[contMasc,contFem], //quantidades encontradas para cada sexo
	        	backgroundColor:[
		            'rgba(255, 99, 132, 0.6)',
		            'rgba(54, 162, 235, 0.6)',
		            'rgba(255, 206, 86, 0.6)',
		            'rgba(75, 192, 192, 0.6)',
		            'rgba(153, 102, 255, 0.6)',
		            'rgba(255, 159, 64, 0.6)',
		            'rgba(255, 99, 132, 0.6)'
	          	],
	            borderWidth:1,
	        	borderColor:'#777',
	        	hoverBorderWidth:3,
	        	hoverBorderColor:'#000'
	        }]
	    },
	    options:{ // opções de definições do gráfico
	    	responsive: true,
	    	maintainAspectRatio: false,
	        title:{
	        	display:true,
	        	text:'Quantidade de pessoas por sexo',
	        	fontSize:25
	        },
	        legend:{
	        	display:false,
	         	position:'bottom',
	         	labels:{
	        		fontColor:'#000'
	          	}
	        },
	        layout:{
	        	padding:{
	        		left:50,
	            	right:0,
	            	bottom:0,
	            	top:0
	          	}
	        },
        	tooltips:{
        		enabled:true
    		}
    	}
	});
}
